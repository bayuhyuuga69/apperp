﻿using System.Threading.Tasks;
using Abp.Application.Services;
using AppERP.Sessions.Dto;

namespace AppERP.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
