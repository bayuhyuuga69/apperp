﻿using System.Threading.Tasks;
using Abp.Application.Services;
using AppERP.Authorization.Accounts.Dto;

namespace AppERP.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
