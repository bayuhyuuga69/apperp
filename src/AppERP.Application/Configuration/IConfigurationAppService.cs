﻿using System.Threading.Tasks;
using Abp.Application.Services;
using AppERP.Configuration.Dto;

namespace AppERP.Configuration
{
    public interface IConfigurationAppService: IApplicationService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}