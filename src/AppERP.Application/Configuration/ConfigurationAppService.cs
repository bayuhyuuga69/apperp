﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using AppERP.Configuration.Dto;

namespace AppERP.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : AppERPAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
