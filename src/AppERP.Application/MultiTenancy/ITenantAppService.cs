﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using AppERP.MultiTenancy.Dto;

namespace AppERP.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
