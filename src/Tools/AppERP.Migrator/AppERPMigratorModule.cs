using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using AppERP.EntityFramework;

namespace AppERP.Migrator
{
    [DependsOn(typeof(AppERPDataModule))]
    public class AppERPMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<AppERPDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}