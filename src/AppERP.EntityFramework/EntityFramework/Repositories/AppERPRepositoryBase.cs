﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace AppERP.EntityFramework.Repositories
{
    public abstract class AppERPRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<AppERPDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected AppERPRepositoryBase(IDbContextProvider<AppERPDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class AppERPRepositoryBase<TEntity> : AppERPRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected AppERPRepositoryBase(IDbContextProvider<AppERPDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
