﻿namespace AppERP
{
    public class AppERPConsts
    {
        public const string LocalizationSourceName = "AppERP";

        public const bool MultiTenancyEnabled = true;
    }
}