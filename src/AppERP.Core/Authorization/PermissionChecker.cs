﻿using Abp.Authorization;
using AppERP.Authorization.Roles;
using AppERP.Authorization.Users;

namespace AppERP.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
