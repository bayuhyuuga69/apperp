﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace AppERP.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : AppERPControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}