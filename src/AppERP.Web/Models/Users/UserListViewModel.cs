﻿using System.Collections.Generic;
using AppERP.Roles.Dto;
using AppERP.Users.Dto;

namespace AppERP.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}