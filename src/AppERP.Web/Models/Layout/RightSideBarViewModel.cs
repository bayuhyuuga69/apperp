using AppERP.Configuration.Ui;

namespace AppERP.Web.Models.Layout
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}