﻿using Abp.Web.Mvc.Views;

namespace AppERP.Web.Views
{
    public abstract class AppERPWebViewPageBase : AppERPWebViewPageBase<dynamic>
    {

    }

    public abstract class AppERPWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected AppERPWebViewPageBase()
        {
            LocalizationSourceName = AppERPConsts.LocalizationSourceName;
        }
    }
}